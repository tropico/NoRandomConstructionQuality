﻿using System;
using System.Reflection;
using RimWorld;
using Verse;

namespace NoRandomConstructionQuality
{
    [StaticConstructorOnStartup]
    public static class Initializer
    {
		static Initializer()
		{
			MethodInfo m1 = typeof(QualityUtility).GetMethod("GenerateQualityCreatedByPawn", BindingFlags.Static | BindingFlags.Public, null, CallingConventions.Standard, new Type[] { typeof(int), typeof(bool) }, null);
            MethodInfo m2 = typeof(_QualityUtility).GetMethod("GenerateQualityCreatedByPawn", BindingFlags.Static | BindingFlags.Public);
            if (Detours.TryDetourFromTo(m1, m2))
            {
                Log.Message("NoRandomConstructionQuality: QualityUtility.RandomCreationQuality overridden successfully!");
            }
        }
    }
}
